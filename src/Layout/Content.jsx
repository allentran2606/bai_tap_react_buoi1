import React, { Component } from "react";

export default class Content extends Component {
  render() {
    return (
      <section className="pt-4 mt-5">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-xl-4 mb-5">
              <div className="card bg-light h-100 px-5 pb-5">
                <div className="feature bg-primary bg-gradient text-white">
                  <i className="bi bi-collection"></i>
                </div>
                <h4 className="font-weight-bold">Fresh new layout</h4>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-xl-4 mb-5">
              <div className="card bg-light h-100 px-5 pb-5">
                <div className="feature bg-primary bg-gradient text-white">
                  <i className="bi bi-collection"></i>
                </div>
                <h4 className="font-weight-bold">Fresh new layout</h4>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-xl-4 mb-5">
              <div className="card bg-light h-100 px-5 pb-5">
                <div className="feature bg-primary bg-gradient text-white">
                  <i className="bi bi-collection"></i>
                </div>
                <h4 className="font-weight-bold">Fresh new layout</h4>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-xl-4 mb-5">
              <div className="card bg-light h-100 px-5 pb-5">
                <div className="feature bg-primary bg-gradient text-white">
                  <i className="bi bi-collection"></i>
                </div>
                <h4 className="font-weight-bold">Fresh new layout</h4>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-xl-4 mb-5">
              <div className="card bg-light h-100 px-5 pb-5">
                <div className="feature bg-primary bg-gradient text-white">
                  <i className="bi bi-collection"></i>
                </div>
                <h4 className="font-weight-bold">Fresh new layout</h4>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-xl-4 mb-5">
              <div className="card bg-light h-100 px-5 pb-5">
                <div className="feature bg-primary bg-gradient text-white">
                  <i className="bi bi-collection"></i>
                </div>
                <h4 className="font-weight-bold">Fresh new layout</h4>
                <p className="mb-0">
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
